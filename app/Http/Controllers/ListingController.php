<?php

namespace App\Http\Controllers;

use App\Listing;
use App\Gallery;
use App\Customer;
use App\Area;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listing = Listing::all();
        $user = DB::table('users')->get();
        $usr = array();
        foreach($user as $i){
            $usr[$i->id] = $i->name;
        }
        $city = array();
        $hometype = array();
        $rooms = array();
        foreach($listing as $i){
            $img = DB::table('galleries')->where('listing_id', $i->id)->first();
            $i->image = $img->image;
            array_push($city, $i->city);
            array_push($hometype, $i->hometype);
            array_push($rooms, $i->rooms);
        }
        $city = array_unique($city);
        $hometype = array_unique($hometype);
        $rooms = array_unique($rooms); 
        $data = ['listing'=>$listing, 'city'=>$city, 'hometype'=>$hometype, 'rooms'=>$rooms, 'img'=>$img->image, 'usr'=>$usr];
        return view('pages/listing')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
