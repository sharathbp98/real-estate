<?php

namespace App\Http\Controllers;

use App\Listing;
use App\Gallery;
use App\Customer;
use App\Area;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class CreateListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        return view('pages/create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = auth()->user()->id;
        $city = $request->city;
        $address = $request->address;
        $rooms = $request->rooms;
        $floorspace = $request->floorspace;
        $price = $request->price;
        $elevator = $request->elevator?true:false;
        $gym = $request->gym?true:false;
        $pool = $request->pool?true:false;
        $basement = $request->basement?true:false;

        $list = Listing::create(["customer_id"=>$user, "city"=>$city, 
                                "address"=>$address, "rooms"=>$rooms,
                                "floorspace"=>$floorspace, "price"=>$price,
                                "elevator"=>$elevator, "gym"=>$gym,
                                "pool"=>$pool, "basement"=>$basement]);

        

        $image = $request->file('image');
        $i = 0;
        if($image){
            foreach($image as $img){
                 $imageName = time().$i.'.'.$img->getClientOriginalExtension();
                 $img->move(public_path('images'), $imageName);  
                 Gallery::create(['image'=>$imageName, 'listing_id'=>$list->id]);
                 $i++;
            }
        }

        return redirect('listing');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
