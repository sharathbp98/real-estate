<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    protected $fillable = ['customer_id', 'city', 'address', 'price', 'rooms', 'floorspace', 'elevator', 'gym', 'pool', 'basement'];

    public function gallery(){
        return $this->belongsTo('App\Gallery', 'foreign_key');
    }

    public function user(){
        return $this->belongTo('App\Customer', 'foreign_key');
    }

}
