<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/listing', 'ListingController@index');
Route::get('/register', 'RegisterController@index'); 
Route::get('/create', 'CreateListingController@index')->middleware('auth');
Route::post('/create', 'CreateListingController@store')->middleware('auth');
Route::get('/view/{id}', 'ViewController@show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
