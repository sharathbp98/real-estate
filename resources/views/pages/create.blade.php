@extends('layouts.main')

@section('title')
  Create listing
@endsection

@section('content')
<div class="container">
  <div class="panel panel-primary">
    <div class="panel-heading"><h2>Upload Listing</h2></div>
    <div class="panel-body">

      @if($message=Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
          <strong>{{$message}}</strong>
      </div>
      <img src="images/{{Session::get('image') }}">
      @endif

      @if(count($errors)>0)
      <div class="alert clert-danger">
        <strong>Whoops!</strong>There were some problems with your input.
        <ul>
          @foreach($errors->all() as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <form action="/create" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group row">
          <label for="state" class="col-sm-2 col-form-label">City</label>
          <div class="col-sm-10">
            <select name="city">
              <option value="Bangalore">Bangalore</option>
              <option value="Mysore">Mysore</option>
              <option value="Mangalore">Mangalore</option>
              <option value="Haasan">Haasan</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="inputAddress">Address</label>
          <textarea name="address" rows="4" class="form-control" id="inputAddress" placeholder="1234 Main St" required></textarea>
        </div>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="rooms">Rooms</label>
            <input type="number" name="rooms" min="1" max="5" required>
          </div>
          <div class="form-group col-md-4">
            <label for="floorspace">Floor Space</label>
            <input type="number" name="floorspace" min="0" max="100000" required>
          </div>
          <div class="form-group col-md-4">
            <label for="price">Price</label>
            <input type="number" name="price" min="0" max="1000000000" required>
          </div>
        </div>

        <div class="form-group">
          
          <div class="form-group form-check col-md-3">
            <input class="form-check-input" type="checkbox" id="elevator" name="elevator">
            <label class="form-check-label" for="elevator" style="margin: 0 20px">Elevator</label>
          </div>
          
          <div class="form-group form-check col-md-3">
            <input class="form-check-input" type="checkbox" id="gym" name="gym">
            <label class="form-check-label" for="gym" style="margin: 0 20px">Gym</label>
          </div>
          <div class="form-group form-check col-md-3">
            <input class="form-check-input" type="checkbox" id="pool" name="pool">
            <label class="form-check-label" for="pool" style="margin: 0 20px">Pool</label>
          </div>
          <div class="form-group form-check col-md-3">
            <input class="form-check-input" type="checkbox" id="basement" name="basement">
            <label class="form-check-label" for="basement" style="margin: 0 20px">Basement</label>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <input type="file" name="image[]" class="form-control" multiple required>
          </div>
        </div>

        <div class="row" style="margin: 20px;text-align: center;">
          <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        <div>
      </form>

    </div>
  </div>
</div>

@endsection
