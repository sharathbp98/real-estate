@extends('layouts.main')

@section('title')
  HomeGuide
@endsection

@section('content')



  <section id="filter" class="">
    <div class="col-sm-2 sidenav" style="background-color: #FAF7F5;">
      <h4>Filter</h4>
      
      <label htmlFor="city">City</label>
      <select id="city" name="city" class="filters neighbourhood" onChange="filter_data()">
        <option value="All">All</option>
        <?php sort($city) ?>
        @foreach($city as $i)
        <option value="{{$i}}">{{$i}}</option>
        @endforeach
      </select>
      
      <label htmlFor="bedrooms">Bedrooms</label>
      <select id="bedrooms" name="bedrooms" class="filters bedrooms" onChange="filter_data()">
        <option value="0">0</option>
        <?php sort($rooms) ?>
        @foreach($rooms as $i)
        <option value="{{$i}}">{{$i}}</option>
        @endforeach 
      </select>
      
      <div class="filters price">
        <span class="title">Price</span>
        <input type="text" id="min_price" name="min_price" class="min-price" onChange="filter_data()" value="0" />
        <input type="text" id="max_price" name="max_price" class="max-price" onChange="filter_data()" value="10000000" />
      </div>
      <div class="filters floor-space">
        <span class="title">Floor space</span>
        <input type="text" id="min_floor_space" name="min_floor_space" class="min-floor-space" onChange="filter_data()" value="0" />
        <input type="text" id="max_floor_space" name="max_floor_space" class="max-floor-space" onChange="filter_data()" value="10000000" />
      </div>
      
      <div class="filters extras">
        <span class="title">
          Extras
        </span>
        <label htmlFor="extras">
          <span>Elevators</span>
          <input id="elevator" name="elevators" value="1" type="checkbox" onChange="filter_data()" />
        </label>
        <label htmlFor="extras">
          <span>Swimming Pool</span>
          <input id="pool" name="swimmingpool" value="1" type="checkbox" onChange="filter_data()" />
        </label>
        <label htmlFor="extras">
          <span>Finished Basement</span>
          <input id="basement" name="finishedbasement" value="1" type="checkbox" onChange="filter_data()" />
        </label>
        <label htmlFor="extras">
          <span>Gym</span>
          <input id="gym" name="gym" value="1" type="checkbox" onChange="filter_data()" />
        </label>
      </div>
    </div>
  </section>

  <section id="listings" class="col-sm-10">
    <section class="search-area">
      <input type="text" name="search" onChange="filter_data()" /> 
    </section>

    <section class="sortby-area">
      <div class="results"><?php echo count($listing) ?> results found</div>
      <div class="sort-options">
        <select id="sortby" name="sortby" class="sortby" onChange="filter_data()">
          <option value="price-dsc">Highest Price</option>
          <option value="price-asc">Lowest Price</option>
        </select>
        <div class="view">
          <i class="fa fa-th-list" aria-hidden="true" onClick={this.props.changeView.bind(null, "long")}></i>
          <i class="fa fa-th" aria-hidden="true" onClick={this.props.changeView.bind(null, "box")}></i>
        </div>
      </div>
    </section>

    <div class="row">
    <section class="listings-results" id="id1">
      
    </section>
    </div>
    
    {{-- <section id="pagination">
      <ul class="pages">
        <li>Prev</li>
        <li class="active">1</li>
        <li>2</li>
        <li>3</li>
        <li>Next</li>
      </ul>
    </section> --}}

  </section>

  <script>
    var obj = {!! json_encode($listing) !!};
    var usr = {!! json_encode($usr) !!};

    function filter_function(lists){
      document.getElementById('id1').innerHTML = "";
      lists.forEach(element => {
        document.getElementById('id1').innerHTML += `
        
          <div class="col-md-12 col-lg-6" key=${element.id}> 
            <div class="listing">
              <div class="listing-img" style="background: url('/images/${element.image}') no-repeat center center;">
                <span class="address">${element.address}</span>
                <div class="details">
                  <div class="user-img">
                  </div>
                  <div class="user-details">
                    <span class="user-name">${usr[element.customer_id]}</span>
                    <span class="post-date">${element.created_at}</span>
                  </div>
                  <div class="listing-details">
                    <div class="floor-space">
                      <i class="fa fa-square-o" aria-hidden="true"></i>
                      ${element.floorspace} ft&sup2;
                    </div>
                    <div class="bedrooms">
                      <i class="fa fa-bed" aria-hidden="true"></i>
                      <span> ${element.rooms} bedrooms</span>
                    </div>
                  </div>
                  <div class="view-btn">
                    <a href="/view/${element.id}" style="text-decoration:none;">View Listing</a>
                  </div>
                </div>
              </div>
              <div class="bottom-info">
                <span class="price">${element.price}</span>
                <span class="location">
                  <i class="fa fa-map-marker" aria-hidden="true"></i>
                  ${element.city}
                </span>
              </div>
            </div>
          </div>
        
        `;
      }); 
    }

    
    function filter_data(){
      var newData = obj;

      if(document.getElementById("city").value != "All"){
        newData = newData.filter((item) => {
          return item.city == document.getElementById("city").value;
        })
      }
      
      newData = newData.filter((item) => {
        return  item.price>=document.getElementById("min_price").value && item.price<=document.getElementById("max_price").value && item.rooms>=document.getElementById("bedrooms").value;
      })
      
      newData = newData.filter((item) => {
        return item.floorspace>=document.getElementById("min_floor_space").value && item.floorspace<=document.getElementById("max_floor_space").value;
      })

      if(document.getElementById('elevator').checked){
        newData = newData.filter((item) => {
          return item.elevator=='1';
        })
      }
      if(document.getElementById('gym').checked){
        newData = newData.filter((item) => {
          return item.gym=='1';
        })
      }
      if(document.getElementById('pool').checked){
        newData = newData.filter((item) => {
          return item.pool=='1';
        })
      }
      if(document.getElementById('basement').checked){
        newData = newData.filter((item) => {
          return item.basement=='1';
        })
      }
      
      if(document.getElementById('sortby').value=='price-asc'){
        newData = newData.sort((a, b)=>{
          return a.price - b.price;
        })
      }
      else{
        newData = newData.sort((a, b)=>{
          return b.price - a.price;
        })
      }

      filter_function(newData);
    }

    filter_function(obj);
  </script>
@endsection