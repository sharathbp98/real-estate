@extends('layouts.main')

@section('title')
  HomeGuide
@endsection

@section('content')
  <div class="col-md-12">  

  <div id="carousel" class="carousel slide" data-ride="carousel">
      <!-- Menu -->
      <ol class="carousel-indicators">
          <li data-target="#carousel" data-slide-to="0" class="active"></li>
          <li data-target="#carousel" data-slide-to="1"></li>
          <li data-target="#carousel" data-slide-to="2"></li>
          <li data-target="#carousel" data-slide-to="3"></li>
          <li data-target="#carousel" data-slide-to="4"></li>
      </ol>
      
      <!-- Items -->
      <div class="carousel-inner" style="height: 800px;width: 100%;">
          
          <div class="item active">
            <a href="/view/24"><img src="/img/index1.jpg" alt="Slide 0" /></a>
          </div>
          
          <div class="item">
            <a href="/view/25"><img src="/img/index2.jpg" alt="Slide 1" /></a>
          </div>

          <div class="item">
            <a href="/view/26"><img src="/img/index3.jpg" alt="Slide 2" /></a>
          </div>
          <div class="item">
            <a href="/view/27"><img src="/img/index4.jpg" alt="Slide 3" /></a>
          </div>
          <div class="item">
            <a href="/view/28"><img src="/img/index5.jpg" alt="Slide 4" /></a>
          </div>
         
      </div> 
      <a href="#carousel" class="left carousel-control" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a href="#carousel" class="right carousel-control" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
  </div>

</div>
@endsection