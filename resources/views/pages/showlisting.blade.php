@extends('layouts.main')

@section('title')
  HomeGuide
@endsection

@section('content')


<div class="col-md-6">  

  <div id="carousel" class="carousel slide" data-ride="carousel">
      <!-- Menu -->
      <ol class="carousel-indicators">
          <li data-target="#carousel" data-slide-to="0" class="active"></li>
          @for($i=1;$i<count($image);$i++)
          <li data-target="#carousel" data-slide-to="{{$i}}"></li>
          @endfor
      </ol>
      
      <!-- Items -->
      <div class="carousel-inner">
          
          <div class="item active">
            <img src="/images/{{$image[0]}}" alt="Slide 0" />
          </div>
          @for($i=1;$i<count($image);$i++)
          <div class="item">
            <img src="/images/{{$image[$i]}}" alt="Slide {{$i}}" />
          </div>
          @endfor
      </div> 
      <a href="#carousel" class="left carousel-control" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a href="#carousel" class="right carousel-control" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
  </div>

</div>

<div class="col-md-6">
  
  <table class="table">
    <h3>Property Details</h3>
    <tr>
      <td>City</td>
      <td>{{$data->city}}</td>
    </tr>
    <tr>
      <td>Address</td>
      <td>{{$data->address}}</td>
    </tr>
    <tr>
      <td>No of Rooms</td>
      <td>{{$data->rooms}}</td>
    </tr>
    <tr>
      <td>Floor Space sq ft</td>
      <td>{{$data->floorspace}}</td>
    </tr>
    <tr>
      <td>Price</td>
      <td><strong>{{$data->price}}</td>
    </tr>
    <tr>
      <td>Elevator</td>
      <td>{{$data->elevator}}</td>
    </tr>
    <tr>
      <td>Gym</td>
      <td>{{$data->gym}}</td>
    </tr>
    <tr>
      <td>Pool</td>
      <td>{{$data->pool}}</td>
    </tr>
    <tr>
      <td>Basement</td>
      <td>{{$data->basement}}</td>
    </tr>
  </table>

  <table class="table">
    <h3>Seller Details</h3>
    <tr>
      <td>Seller Name</td>
      <td>{{$seller->name}}
    </tr>
    <tr>
      <td>Email</td>
      <td>{{$seller->email}}</td>
    </tr>
  </table>
</div>
  
  

  

@endsection