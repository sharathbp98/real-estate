<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>@yield('title')</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="/css/app.css" rel="stylesheet">
  
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
    integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  
  <script src="/js/jquery.js"></script>
  <script src="/js/app.js"></script>
</head>


<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid heading">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="/">HomeGuide</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="/listing">Buy</a></li>
        <li><a href="/create">Sell</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        @if(! Auth::check())
        <li><a href="/login"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
        @else
        <form method="POST" action="/logout">
          {{ csrf_field() }}
          <li><button class="glyphicon glyphicon-log-in" type="submit" style="color: white; background-color: Transparent; border: none; padding: 15px;">&nbspLogout</button></li>
        </form>
        @endif
      </ul>
    </div>
  </div>
</nav>



<section id="content-area">
  @yield('content')
</section>
        
</body>
</html>