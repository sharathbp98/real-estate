<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id')->unsigned();
            $table->string('city')->unique();
            $table->string('neighbourhood')->unique();
            $table->string('address');
            $table->integer('rooms');
            $table->integer('floorspace');
            $table->integer('price');
            $table->boolean('elevator')->default(0);
            $table->boolean('gym')->default(0);
            $table->boolean('pool')->default(0);
            $table->boolean('basement')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
